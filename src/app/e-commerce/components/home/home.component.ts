import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../services/categories.service';
import { ProductsService } from '../../services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'turing-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  imagesUrl = environment.imagesUrl;
  categories = {};
  products = {};
  constructor(
    private categoriesService: CategoriesService,
    private productsService: ProductsService) {
    /**
     * This method will bring the categories.
     */
    this.categoriesService.getCategories()
    .subscribe(categories => {
      this.categories = categories.rows;
    });

    /**
     * This method will bring the products.
     */
    this.productsService.getProducts()
      .subscribe(products => {
        this.products = products.rows;
    });
  }

  ngOnInit() {
  }

}
