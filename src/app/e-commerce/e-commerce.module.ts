import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { ECommerceRoutingModule } from './e-commerce-routing.module';
import { CategoriesService } from './services/categories.service';
import { ProductsService } from './services/products.service';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    ECommerceRoutingModule,
  ],
  providers: [
    CategoriesService,
    ProductsService,
  ]
})
export class ECommerceModule { }
