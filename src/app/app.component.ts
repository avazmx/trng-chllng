import { Component } from '@angular/core';

@Component({
  selector: 'turing-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { }
