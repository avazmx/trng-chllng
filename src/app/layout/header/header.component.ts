import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'turing-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerName = 'SHOPMATE';
  constructor(public router: Router) { }
  ngOnInit() { }

  /**
   * This method will return you to the Home Page
   */
  redirectToHome() {
    this.router.navigate(['/home']);
  }

}
